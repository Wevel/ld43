﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building
{
    public enum BuiltState
    {
        Started,
        Partial,
        Completed,
        Destroyed,
    }

    public static readonly float minBuildingWidth;

    private static Dictionary<string, BuildingType> buildingTypes = new Dictionary<string, BuildingType> ();

    public static IEnumerable<BuildingType> BuildingTypes
    {
        get
        {
            foreach (KeyValuePair<string, BuildingType> item in buildingTypes) yield return item.Value;
        }
    }

    static Building ()
    {
        addBuildingType ("Ritual Site", 1f, 0.15f, 0, true, 
            (type, map, position) =>
            {
                return new Building_Sacrifice_RitualSite (map, type, type.name, position, type.width, entranceOffset: 1f / 8f, killOnDestroy: false, powerGianModifier: 0.5f, minimumWealthLevel: type.requiredWealthLevel);
            },
            (type, map, person) => 
            {
                float weight = 0;

                if (person.IsFollower)
                {
                    weight += 0.5f;
                    weight += (map.spareSacrificeBuildingCount - (0.5f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount)) * 0.4f;
                }
                else
                {
                    weight -= (0.2f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount);
                }

                return weight;
            });

        addBuildingType ("Temple", 2f, 0.5f, 1, true, 
            (type, map, position) =>
            {
                return new Building_Sacrifice_Temple (map, type, type.name, position, type.width, entranceOffset: 1f / 8f, killOnDestroy: true, powerGianModifier: 1.0f, minimumWealthLevel: type.requiredWealthLevel);
            },
            (type, map, person) =>
            {
                float weight = 0;
                if (person.IsFollower)
                {
                    weight += 2.75f;
                    weight += (map.spareSacrificeBuildingCount - (0.5f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount)) * 0.4f;
                }
                else
                {
                    weight -= (0.2f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount);
                }

                return weight;
            });

        addBuildingType ("Small Pyramid", 3.75f, 0.75f, 2, true, 
            (type, map, position) =>
            {
                return new Building_Sacrifice_SmallPyramid (map, type, type.name, position, type.width, entranceOffset: 0, killOnDestroy: true, powerGianModifier: 2.5f, minimumWealthLevel: type.requiredWealthLevel);
            },
            (type, map, person) =>
            {
                float weight = 0;
                if (person.IsFollower)
                {
                    weight += 4.0f;
                    weight += (map.spareSacrificeBuildingCount - (0.5f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount)) * 0.4f;
                }
                else
                {
                    weight -= (0.2f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount);
                }

                return weight;
            });

        addBuildingType ("Large Pyramid", 7f, 1.65f, 3, true, 
            (type, map, position) =>
            {
                return new Building_Sacrifice_LargePyramid (map, type, type.name, position, type.width, entranceOffset: 0, killOnDestroy: true, powerGianModifier: 10.0f, minimumWealthLevel: type.requiredWealthLevel);
            },
            (type, map, person) =>
            {
                float weight = 0;
                if (person.IsFollower)
                {
                    weight += 15.0f;
                    weight += (map.spareSacrificeBuildingCount - (0.5f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount)) * 0.4f;
                }
                else
                {
                    weight -= (0.2f * map.spareSacrificeBuildingCount * map.spareSacrificeBuildingCount);
                }
                return weight;
            });

        addBuildingType ("Shack", 1f, 0.05f, 0, false,
           (type, map, position) =>
           {
               return new Building_House (map, type, type.name, position, type.width, entranceOffset: 0, killOnDestroy: true, minimumWealthLevel: type.requiredWealthLevel, maxOwners: 3, spawnPersonDelay: 10);
           },
           (type, map, person) =>
           {
               return 0.15f * (map.peopleWithNoHouse - map.freeBuildingSpaced + 2);
           });

        addBuildingType ("House", 2f, 0.35f, 1, false,
          (type, map, position) =>
          {
              return new Building_House (map, type, type.name, position, type.width, entranceOffset: 0, killOnDestroy: true, minimumWealthLevel: type.requiredWealthLevel, maxOwners: 5, spawnPersonDelay: 15);
          },
          (type, map, person) =>
          {
              float weight = 0.175f * (map.peopleWithNoHouse - map.freeBuildingSpaced + 4);
              if (person.house == null || person.house.minimumWealthLevel < person.wealthLevel) weight += 1;
              return weight;
          });

        addBuildingType ("Mansion", 2f, 0.65f, 2, false,
          (type, map, position) =>
          {
              return new Building_House (map, type, type.name, position, type.width, entranceOffset: 0, killOnDestroy: true, minimumWealthLevel: type.requiredWealthLevel, maxOwners: 8, spawnPersonDelay: 20);
          },
          (type, map, person) =>
          {
              float weight = 0.25f * (map.peopleWithNoHouse - map.freeBuildingSpaced + 6);
              if (person.house == null || person.house.minimumWealthLevel < person.wealthLevel) weight += 1;
              return weight;
          });

        //addBuildingType ("Palace", 8f, 1.45f, 3, false,
        //  (type, map, position) =>
        //  {
        //      return new Building_House (map, type, type.name, position, type.width, entranceOffset: 0, killOnDestroy: true, minimumWealthLevel: type.requiredWealthLevel, maxOwners: 11, spawnPersonDelay: 25);
        //  },
        //  (type, map, person) =>
        //  {
        //      float weight = 0.15f * (map.peopleWithNoHouse - map.freeBuildingSpaced + 8);
        //      if (person.house == null || person.house.minimumWealthLevel < person.wealthLevel) weight += 1;
        //      return weight;
        //  });

        minBuildingWidth = 10f;
        foreach (KeyValuePair<string, BuildingType> item in buildingTypes)
        {
            if (item.Value.width < minBuildingWidth) minBuildingWidth = item.Value.width;
        }
    }

    public static BuildingType GetType (string name)
    {
        BuildingType type;
        if (buildingTypes.TryGetValue (name, out type)) return type;
        else Debug.LogError ("No buildingtype with name: " + name);
        return null;
    }

    private static void addBuildingType (string name, float width, float upFrontBuildCost, int requiredWealthLevel, bool followerBuilding, BuildingType.CreateBuilding createBuilding, BuildingType.GetBuildWeight getBuildWeight)
    {
        if (buildingTypes.ContainsKey (name)) Debug.LogError ("Already a building with name: " + name);
        else buildingTypes[name] = new BuildingType (name, width, upFrontBuildCost, requiredWealthLevel, followerBuilding, createBuilding, getBuildWeight);
    }

    protected readonly Map map;
    protected readonly BuildingType type;

    public readonly string name;
    public readonly float minX;
    public readonly float maxX;
    public readonly float width;
    public readonly float position;
    public readonly float entranceOffset;
    public readonly bool killOnDestroy;
    public readonly int minimumWealthLevel;
    public readonly float verticalOffset;

    public float buildPercent;
    
    public BuildingDisplayType display;

    public Person currentPerson { get; private set; }

    public BuiltState builtState { get; private set; }

    public bool IsInUse
    {
        get
        {
            return currentPerson != null;
        }
    }

    protected Building (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, int minimumWealthLevel, float verticalOffset)
    {
        this.map = map;
        this.type = type;
        this.name = name;
        this.position = Mathf.Floor (position / map.pixelSize) * map.pixelSize;
        this.minimumWealthLevel = minimumWealthLevel;
        this.entranceOffset = entranceOffset;
        this.killOnDestroy = killOnDestroy;
        this.builtState = BuiltState.Started;
        this.verticalOffset = verticalOffset;
        buildPercent = 0;

        float halfSize = width / 2f;
        minX = Mathf.Floor ((this.position - halfSize) / map.pixelSize) * map.pixelSize;
        maxX = Mathf.Floor ((this.position + halfSize) / map.pixelSize) * map.pixelSize;
        this.width = maxX - minX;
    }

    public void InstantBuild ()
    {
        if (builtState == BuiltState.Started || builtState == BuiltState.Partial)
        {
            builtState = BuiltState.Completed;
            buildPercent = 1;
        }
    }

    internal void FinishBuildWork (Person owner)
    { 
        switch (builtState)
        {
            case BuiltState.Started:
                owner.AddWealth (-type.upfrontBuildCost);
                builtState = BuiltState.Partial;
                buildPercent = 0;
                break;
            case BuiltState.Partial:
                owner.AddWealth (-type.upfrontBuildCost);
                builtState = BuiltState.Completed;
                buildPercent = 1;
                break;
            case BuiltState.Completed:
            case BuiltState.Destroyed:
            default:
                break;
        }
    }

    public virtual void Update (float deltaTime)
    {
        if (currentPerson != null && !currentPerson.alive) currentPerson = null;
    }

    public virtual float GetClosestEntrance (Person person)
    {
        if (person.position < position) return position - entranceOffset;
        else return position + entranceOffset;
    }

    public virtual bool TryUseBuilding (Person person)
    {
        if (currentPerson != null) return false;
        if (builtState == BuiltState.Destroyed) return false;

        currentPerson = person;
        if (builtState == BuiltState.Completed) PersonEnteredBuilding (person);

        return true;
    }

    public virtual void FinishUseBuilding (Person person)
    {
        if (currentPerson == person || currentPerson == null)
        {
            currentPerson = null;
            person.hidden = false;
        }
        else
        {
            //Debug.LogError ("Different person using building: " + this.GetType ());
            Debug.Log ("Different person using building: " + this.GetType ());
        }
    }

    public void Destroy ()
    {
        builtState = BuiltState.Destroyed;
        map.RemoveBuilding (this);
        if (killOnDestroy && currentPerson != null) currentPerson.Kill ();
    }

    public bool DoesOverlap (Building other)
    {
        return (other.minX >= this.minX && other.minX < this.maxX)
            || (other.maxX > this.minX && other.maxX <= this.maxX);
    }

    protected abstract void PersonEnteredBuilding (Person person);
}

public class Building_House : Building
{
    public readonly int maxOwners;
    private readonly float spawnPersonDelay;

    private List<Person> owners = new List<Person> ();

    private float spawnPersonTimer;

    public bool IsSpace
    {
        get
        {
            return owners.Count < maxOwners;
        }
    }

    public Building_House (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, int minimumWealthLevel, int maxOwners, float spawnPersonDelay)
        : base (map, type, name, position, width, entranceOffset, killOnDestroy, minimumWealthLevel, 0)
    {
        this.maxOwners = maxOwners;
        this.spawnPersonDelay = spawnPersonDelay;
        this.spawnPersonTimer = Random.Range (spawnPersonDelay, spawnPersonDelay * 1.1f);
    }

    public override void Update (float deltaTime)
    {
        base.Update (deltaTime);

        for (int i = owners.Count - 1; i >= 0; i--)
        {
            if (!owners[i].alive) owners.RemoveAt (i);
        }

        if (builtState == BuiltState.Completed)
        {
            if (IsSpace)
            {
                if (spawnPersonTimer > 0) spawnPersonTimer -= deltaTime;
                else
                {
                    spawnPersonTimer = Random.Range (spawnPersonDelay, spawnPersonDelay * 1.1f);
                    map.AddPerson (position, minimumWealthLevel);
                }
            }
        }

        if (IsSpace) map.freeBuildingSpaced += maxOwners - owners.Count;
    }

    public override bool TryUseBuilding (Person person)
    {
        if (builtState == BuiltState.Destroyed) return false;
        if (builtState == BuiltState.Completed)
        {
            if (!owners.Contains (person)) return false;
            PersonEnteredBuilding (person);
        }

        return true;
    }

    protected override void PersonEnteredBuilding (Person person)
    {
        person.hidden = true;
    }

    public bool TryAddOwner (Person person)
    {
        if (owners.Contains (person)) return true;
        if (owners.Count < maxOwners)
        {
            spawnPersonTimer = Random.Range (spawnPersonDelay, spawnPersonDelay * 1.1f);
            owners.Add (person);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RemoveOwner (Person person)
    {
        if (owners.Contains (person)) owners.Remove (person);
    }
}

public abstract class Building_Sacrifice : Building
{
    public readonly float powerGianModifier;

    public Building_Sacrifice (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, float powerGianModifier, int minimumWealthLevel, float verticalOffset)
        : base (map, type, name, position, width, entranceOffset, killOnDestroy, minimumWealthLevel, verticalOffset)
    {
        this.powerGianModifier = powerGianModifier;
    }

    protected override void PersonEnteredBuilding (Person person)
    {
        person.hidden = true;
        map.startCoroutine (MakeSacrifice (person));
    }

    protected abstract IEnumerator MakeSacrifice (Person person);
}

public class Building_Sacrifice_RitualSite : Building_Sacrifice
{
    public Building_Sacrifice_RitualSite (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, float powerGianModifier, int minimumWealthLevel) 
        : base (map, type, name, position, width, entranceOffset, killOnDestroy, powerGianModifier, minimumWealthLevel, 0) { }

    protected override IEnumerator MakeSacrifice (Person person)
    {
        // ToDo: Sacrifice animation

        yield return null;

        map.FinishSacrifice (person, this);
        person.FinishSacrifice ();
        FinishUseBuilding (person);
    }
}

public class Building_Sacrifice_Temple : Building_Sacrifice
{
    public Building_Sacrifice_Temple (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, float powerGianModifier, int minimumWealthLevel)
        : base (map, type, name, position, width, entranceOffset, killOnDestroy, powerGianModifier, minimumWealthLevel, 0) { }

    protected override IEnumerator MakeSacrifice (Person person)
    {
        // ToDo: Sacrifice animation

        yield return null;
        map.FinishSacrifice (person, this);
        person.FinishSacrifice ();
        FinishUseBuilding (person);
    }
}

public class Building_Sacrifice_SmallPyramid : Building_Sacrifice
{
    public Building_Sacrifice_SmallPyramid (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, float powerGianModifier, int minimumWealthLevel)
        : base (map, type, name, position, width, entranceOffset, killOnDestroy, powerGianModifier, minimumWealthLevel, -Random.Range(0, 4) * map.pixelSize) { }

    protected override IEnumerator MakeSacrifice (Person person)
    {
        // ToDo: Sacrifice animation

        yield return null;
        map.FinishSacrifice (person, this);
        person.FinishSacrifice ();
        FinishUseBuilding (person);
    }

    public override float GetClosestEntrance (Person person)
    {
        return position;
    }
}

public class Building_Sacrifice_LargePyramid : Building_Sacrifice
{
    public Building_Sacrifice_LargePyramid (Map map, BuildingType type, string name, float position, float width, float entranceOffset, bool killOnDestroy, float powerGianModifier, int minimumWealthLevel)
        : base (map, type, name, position, width, entranceOffset, killOnDestroy, powerGianModifier, minimumWealthLevel, -Random.Range (0, 8) * map.pixelSize) { }

    protected override IEnumerator MakeSacrifice (Person person)
    {
        // ToDo: Sacrifice animation

        yield return null;
        map.FinishSacrifice (person, this);
        person.FinishSacrifice ();
        FinishUseBuilding (person);
    }

    public override float GetClosestEntrance (Person person)
    {
        return position;
    }
}