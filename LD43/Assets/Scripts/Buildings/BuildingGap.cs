﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGap
{
    public readonly float minX;
    public readonly float maxX;

    public readonly float position;
    public readonly float width;

    public readonly float originDistance;

    public BuildingGap (float position, float width)
    {
        this.position = position;
        this.width = width;

        minX = position - (width / 2);
        maxX = position + (width / 2);

        originDistance = Mathf.Min (Mathf.Abs (minX), Mathf.Abs (maxX));
    }

    public bool Contains (Building building)
    {
        return minX <= building.minX && maxX >= building.maxX;
    }
}
