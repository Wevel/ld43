﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingType
{
    public delegate Building CreateBuilding (BuildingType type, Map map, float position);
    public delegate float GetBuildWeight (BuildingType type, Map map, Person person);

    public readonly string name;
    public readonly float width;
    public readonly float upfrontBuildCost;
    public readonly int requiredWealthLevel;
    public readonly bool followerBuilding;
    private readonly CreateBuilding createBuilding;
    private readonly GetBuildWeight getBuildWeight;

    public BuildingDisplayType displayType;

    public BuildingType (string name, float width, float upfrontBuildCost, int requiredWealthLevel, bool followerBuilding, CreateBuilding createBuilding, GetBuildWeight getBuildWeight)
    {
        this.name = name;
        this.width = width;
        this.upfrontBuildCost = upfrontBuildCost;
        this.requiredWealthLevel = requiredWealthLevel;
        this.followerBuilding = followerBuilding;
        this.createBuilding = createBuilding;
        this.getBuildWeight = getBuildWeight;
    }

    public Building CreateBuildingInstance (Map map, float position)
    {
        Building building = createBuilding (this, map, position);
        building.display = displayType;
        return building;
    }

    public float GetBuildDecisionWeight (Map map, Person person)
    {
        // The weight specific to that building
        float weight = getBuildWeight (this, map, person);

        // Take acount for the cost
        // We are over valuing building a building as it required the same cost twice more to finish the build
        weight -= upfrontBuildCost * 3;

        if (person.IsFollower)
        {
            // Make followers more likely to build follower buildings
            if (followerBuilding) weight += 0.25f;
        }
        else
        {
            if (followerBuilding) return float.NegativeInfinity;
        }

        // Push people to building the type of building for their wealth level, and that they have the wealth to use
        if (person.wealthLevel < requiredWealthLevel) return float.NegativeInfinity;
        else if (person.wealthLevel > requiredWealthLevel) weight -= (person.wealthLevel - requiredWealthLevel - 1) * 0.5f;
        else weight += 0.25f;

        return weight;
    }
}
