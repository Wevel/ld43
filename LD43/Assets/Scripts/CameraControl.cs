﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float spoolSpeed = 5;
    public Game game;

    private Vector3 lastPosition;
    private float height;
    public float speed = 15;

    private void Awake ()
    {
        height = transform.position.y;
    }

    void Update ()
    {
        Camera mainCamera = Camera.main;

        if (mainCamera != null)
        {
            if (game.state == GameState.Playing)
            {
                // Make sure that this only happenes if the mouse is over the screen
                if (mainCamera.pixelRect.Contains (Input.mousePosition))
                {
                    // Reset the 'lastPosition' when the mouse button is first pressed down
                    if (Input.GetMouseButtonDown (2)) lastPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

                    //Pan
                    Vector3 currentPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

                    if (Input.GetMouseButton (2)) mainCamera.transform.Translate (lastPosition - currentPosition);

                    lastPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);
                }
            }
            

            float delta = Mathf.Clamp (Input.GetAxis ("Horizontal"), -0.5f, 1.0f) * speed * Time.deltaTime;

            if (delta > 0 && delta < game.pixelSize) delta = game.pixelSize;
            else if (delta < 0 && delta > -game.pixelSize) delta = -game.pixelSize;

            Vector3 pos = mainCamera.transform.position;
            pos.x = Mathf.FloorToInt ((pos.x + delta) / game.pixelSize) * game.pixelSize;
            pos.y = height;
            mainCamera.transform.position = pos;
        }        
    }

    internal void ResetPosition ()
    {
        Camera mainCamera = Camera.main;
        mainCamera.transform.position = new Vector3 (0, mainCamera.transform.position.y, mainCamera.transform.position.z);
    }
}
