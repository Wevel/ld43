﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildingDisplayType
{
    public string name;
    public Sprite backSprite;
    public Sprite frontSprite;

    [System.NonSerialized]
    public MapRenderer.SpriteDrawData backSpriteDrawData;

    [System.NonSerialized]
    public MapRenderer.SpriteDrawData frontSpriteDrawData;

    public void GenerateDrawData ()
    {
        if (backSprite != null) backSpriteDrawData = new MapRenderer.SpriteDrawData (backSprite);
        if (frontSprite != null) frontSpriteDrawData = new MapRenderer.SpriteDrawData (frontSprite);
    }
}