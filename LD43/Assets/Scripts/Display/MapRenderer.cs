﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class MapRenderer : MonoBehaviour
{
    private enum State
    {
        Default = 0,
        WealthMap,
        BeliefMap
    }

    public struct SpriteDrawData
    {
        public float uMin;
        public float uMax;
        public float vMin;
        public float vMax;

        public float xMin;
        public float xMax;
        public float yMin;
        public float yMax;

        public readonly Sprite sprite;

        public SpriteDrawData (Sprite sprite)
        {
            this.sprite = sprite;
            Rect r = sprite.rect;

            uMin = r.xMin / (float)sprite.texture.width;
            uMax = r.xMax / (float)sprite.texture.width;
            vMin = r.yMin / (float)sprite.texture.height;
            vMax = r.yMax / (float)sprite.texture.height;

            float xOff = sprite.pivot.x / sprite.pixelsPerUnit;
            float yOff = sprite.pivot.y / sprite.pixelsPerUnit;

            xMin = -xOff;
            xMax = (r.width / sprite.pixelsPerUnit) - xOff;
            yMin = -yOff;
            yMax = (r.height / sprite.pixelsPerUnit) - yOff;
        }

        public void DrawGLPoints (float x, float y, float z)
        {
            GL.TexCoord2 (uMin, vMin);
            GL.Vertex3 (x + xMin, y + yMin, z);

            GL.TexCoord2 (uMin, vMax);
            GL.Vertex3 (x + xMin, y + yMax, z);

            GL.TexCoord2 (uMax, vMax);
            GL.Vertex3 (x + xMax, y + yMax, z);

            GL.TexCoord2 (uMax, vMin);
            GL.Vertex3 (x + xMax, y + yMin, z);
        }
    }

    public Material simpleWealthModeMaterial;
    public Material simpleBeliefModeMaterial;
    public Material cloudMatierls;
    public Material buildingStartedMaterials;
    public Material buildingPartialMaterials;
    public Material buildingMaterials;
    public Material particleMaterial;

    public PixelParticleSystem pixelParticleSystem;
    private SpriteDrawData particleSpriteData;

    public Material groundMaterial;
    public Material peopleMaterial;
    public int initialDataTextureSize = 8;

    public List<BuildingDisplayType> buildingTypes = new List<BuildingDisplayType> ();

    private float positionScale = 10;
    private Texture2D peopleDataTexture;
    private Color[] dataColours;
    private int maxPeople;

    public Game game;
    private Map map;

    private bool rescalePosition = false;
    private float newPositionScale;

    private bool increaceTextureSize = false;

    private State currentState = State.Default;

    private float cloudsTime;

    private void Awake ()
    {
        createNewPeopleDataTexture (initialDataTextureSize);
        peopleMaterial.SetFloat ("_PositionScale", positionScale);
        simpleWealthModeMaterial.SetFloat ("_PositionScale", positionScale);
        simpleBeliefModeMaterial.SetFloat ("_PositionScale", positionScale);

        for (int i = 0; i < buildingTypes.Count; i++) buildingTypes[i].GenerateDrawData ();
    }

    private void Start ()
    {
        particleSpriteData = new SpriteDrawData (pixelParticleSystem.sprite);
        particleMaterial.mainTexture = pixelParticleSystem.sprite.texture;
    }

    private void Update ()
    {
        if (cloudsTime > 0) cloudsTime += Time.deltaTime * game.timeSpeed;
        else cloudsTime -= Time.deltaTime * game.timeSpeed;
        cloudMatierls.SetFloat ("_TimeOffset", cloudsTime);

        if (map != null)
        {
            if (game.state == GameState.Playing)
            {
                if (Input.GetKeyDown (KeyCode.Q)) currentState = State.Default;
                else if (Input.GetKeyDown (KeyCode.W)) currentState = State.WealthMap;
                else if (Input.GetKeyDown (KeyCode.E)) currentState = State.BeliefMap;
            }

            if (rescalePosition)
            {
                rescalePosition = false;
                positionScale = newPositionScale;
                peopleMaterial.SetFloat ("_PositionScale", positionScale);
                simpleWealthModeMaterial.SetFloat ("_PositionScale", positionScale);
                simpleBeliefModeMaterial.SetFloat ("_PositionScale", positionScale);
            }

            if (increaceTextureSize)
            {
                increaceTextureSize = false;
                createNewPeopleDataTexture (peopleDataTexture.width * 2);
            }


            switch (currentState)
            {
                case State.BeliefMap:
                    updateBeliefMapColours ();
                    break;
                case State.WealthMap:
                case State.Default:
                default:
                    updateDefaultMapColours ();
                    break;
            }

            peopleDataTexture.SetPixels (dataColours);
            peopleDataTexture.Apply ();
        }
    }

    private void OnDestroy ()
    {
        if (peopleDataTexture != null) Destroy (peopleDataTexture);
    }

    private void OnRenderObject ()
    {
        if (map != null)
        {
            Camera camera = Camera.current;
            if ((~camera.cullingMask & (1 << gameObject.layer)) != 0) return;

            Vector3 cameraPosition = camera.transform.position;
            float halfHeight = camera.orthographicSize;
            float halfWidth = camera.orthographicSize * camera.aspect;

            #region Draw Ground

            groundMaterial.SetPass (0);

            GL.Begin (GL.QUADS);
            GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y - halfHeight, 1);
            GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y + halfHeight, 1);
            GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y + halfHeight, 1);
            GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y - halfHeight, 1);
            GL.End ();

            cloudMatierls.SetPass (0);

            GL.Begin (GL.QUADS);
            GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y - halfHeight, 0.5f);
            GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y + halfHeight, 0.5f);
            GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y + halfHeight, 0.5f);
            GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y - halfHeight, 0.5f);
            GL.End ();

            #endregion
            #region Draw Building Backs

            buildingStartedMaterials.SetPass (0);
            GL.Begin (GL.QUADS);

            foreach (Building item in map.Buildings)
            {
                if (item.display != null && item.display.backSprite != null && item.builtState == Building.BuiltState.Started) item.display.backSpriteDrawData.DrawGLPoints (item.position, item.verticalOffset, 0);
            }

            GL.End ();

            buildingPartialMaterials.SetPass (0);
            GL.Begin (GL.QUADS);

            foreach (Building item in map.Buildings)
            {
                if (item.display != null && item.display.backSprite != null && item.builtState == Building.BuiltState.Partial) item.display.backSpriteDrawData.DrawGLPoints (item.position, item.verticalOffset, 0);
            }

            GL.End ();

            buildingMaterials.SetPass (0);
            GL.Begin (GL.QUADS);

            foreach (Building item in map.Buildings)
            {
                if (item.display != null && item.display.backSprite != null && item.builtState == Building.BuiltState.Completed) item.display.backSpriteDrawData.DrawGLPoints (item.position, item.verticalOffset, 0);
            }

            GL.End ();

            #endregion
            #region Draw People

            // We want to draw people so they look like they go into/though buildings
            if (currentState == State.Default)
            {
                peopleMaterial.SetPass (0);

                GL.Begin (GL.QUADS);
                GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y - halfHeight, -1);
                GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y + halfHeight, -1);
                GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y + halfHeight, -1);
                GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y - halfHeight, -1);
                GL.End ();
            }

            #endregion
            #region Draw Building Fronts

            buildingStartedMaterials.SetPass (0);
            GL.Begin (GL.QUADS);

            foreach (Building item in map.Buildings)
            {
                if (item.display != null && item.display.frontSprite != null && item.builtState == Building.BuiltState.Started) item.display.frontSpriteDrawData.DrawGLPoints (item.position, item.verticalOffset, -2);
            }

            GL.End ();

            buildingPartialMaterials.SetPass (0);
            GL.Begin (GL.QUADS);

            foreach (Building item in map.Buildings)
            {
                if (item.display != null && item.display.frontSprite != null && item.builtState == Building.BuiltState.Partial) item.display.frontSpriteDrawData.DrawGLPoints (item.position, item.verticalOffset, -2);
            }

            GL.End ();

            buildingMaterials.SetPass (0);
            GL.Begin (GL.QUADS);

            foreach (Building item in map.Buildings)
            {
                if (item.display != null && item.display.frontSprite != null && item.builtState == Building.BuiltState.Completed) item.display.frontSpriteDrawData.DrawGLPoints (item.position, item.verticalOffset, -2);
            }

            GL.End ();

            #endregion
            #region Draw Map Modes

            // We want to draw the map modes so that you can still see them for people who are inside buildings
            if (currentState == State.WealthMap)
            {
                simpleWealthModeMaterial.SetPass (0);
                GL.Begin (GL.QUADS);
                GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y - halfHeight, -1);
                GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y + halfHeight, -1);
                GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y + halfHeight, -1);
                GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y - halfHeight, -1);
                GL.End ();
            }
            else if (currentState == State.BeliefMap)
            {
                simpleBeliefModeMaterial.SetPass (0);
                GL.Begin (GL.QUADS);
                GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y - halfHeight, -1);
                GL.Vertex3 (cameraPosition.x - halfWidth, cameraPosition.y + halfHeight, -1);
                GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y + halfHeight, -1);
                GL.Vertex3 (cameraPosition.x + halfWidth, cameraPosition.y - halfHeight, -1);
                GL.End ();
            }

            #endregion
            #region Draw Particles

            particleMaterial.SetPass (0);
            GL.Begin (GL.QUADS);
            particleSpriteData.DrawGLPoints (camera.transform.position.x, camera.transform.position.y, -1.5f);
            GL.End ();

            #endregion

            GL.Flush ();
        }
    }

    public void RandomiseClouds ()
    {
        if (Random.value < 0.5f) cloudsTime = Random.Range (0.1f, 1000f);
        else cloudsTime = -Random.Range (0.1f, 1000f);
        cloudMatierls.SetFloat ("_TimeOffset", cloudsTime);
    }

    private void updateDefaultMapColours ()
    {
        newPositionScale = positionScale;

        float absPosition;
        int totalPeople = 0;
        foreach (Person item in map.People)
        {
            if (!item.hidden)
            {
                absPosition = Mathf.Abs (item.position);
                if (absPosition < positionScale)
                {
                    if (totalPeople < maxPeople)
                    {
                        dataColours[totalPeople].r = 0.5f + (item.position / positionScale / 2f);
                        dataColours[totalPeople].g = item.wealthCoefficient;
                        dataColours[totalPeople].b = item.noisePosition.x;
                        dataColours[totalPeople].a = item.noisePosition.y;
                        totalPeople++;
                    }
                    else
                    {
                        increaceTextureSize = true;
                    }
                }
                else
                {
                    rescalePosition = true;
                    if (absPosition > newPositionScale) newPositionScale = absPosition;
                }
            }
        }

        peopleMaterial.SetInt ("_PersonCount", totalPeople);
        simpleWealthModeMaterial.SetInt ("_PersonCount", totalPeople);
        simpleBeliefModeMaterial.SetInt ("_PersonCount", totalPeople);
    }

    private void updateBeliefMapColours ()
    {
        newPositionScale = positionScale;

        float absPosition;
        int totalPeople = 0;
        foreach (Person item in map.People)
        {
            if (!item.hidden)
            {
                absPosition = Mathf.Abs (item.position);
                if (absPosition < positionScale)
                {
                    if (totalPeople < maxPeople)
                    {
                        dataColours[totalPeople].r = 0.5f + (item.position / positionScale / 2f);
                        dataColours[totalPeople].g = item.belief;
                        dataColours[totalPeople].b = item.noisePosition.x;
                        dataColours[totalPeople].a = item.noisePosition.y;
                        totalPeople++;
                    }
                    else
                    {
                        increaceTextureSize = true;
                    }
                }
                else
                {
                    rescalePosition = true;
                    if (absPosition > newPositionScale) newPositionScale = absPosition;
                }
            }
        }

        peopleMaterial.SetInt ("_PersonCount", totalPeople);
        simpleWealthModeMaterial.SetInt ("_PersonCount", totalPeople);
        simpleBeliefModeMaterial.SetInt ("_PersonCount", totalPeople);
    }

    public void SetMap (Map map)
    {
        this.map = map;
    }

    private void createNewPeopleDataTexture (int size)
    {
        if (peopleDataTexture != null) Destroy (peopleDataTexture);

        peopleDataTexture = new Texture2D (size, size, GraphicsFormat.R16G16B16A16_SFloat, TextureCreationFlags.None);
        peopleDataTexture.wrapMode = TextureWrapMode.Clamp;
        peopleDataTexture.filterMode = FilterMode.Point;

        dataColours = peopleDataTexture.GetPixels ();
        peopleMaterial.SetTexture ("_DataTex", peopleDataTexture);
        peopleMaterial.SetInt ("_DataTextureSize", size);

        simpleWealthModeMaterial.SetTexture ("_DataTex", peopleDataTexture);
        simpleWealthModeMaterial.SetInt ("_DataTextureSize", size);

        simpleBeliefModeMaterial.SetTexture ("_DataTex", peopleDataTexture);
        simpleBeliefModeMaterial.SetInt ("_DataTextureSize", size);

        maxPeople = size * size;
    }

    private void OnDrawGizmos ()
    {
        if (map != null)
        {
            Gizmos.color = Color.red;

            foreach (Building item in map.Buildings)
            {
                Gizmos.DrawLine (new Vector3 (item.minX, -2), new Vector3 (item.minX, 2));
                Gizmos.DrawLine (new Vector3 (item.maxX, -2), new Vector3 (item.maxX, 2));
            }

            Gizmos.color = Color.cyan;

            for (int i = 0; i < map.gaps.Count; i++)
            {
                Gizmos.DrawLine (new Vector3 (map.gaps[i].minX, -1), new Vector3 (map.gaps[i].minX, 1));
                Gizmos.DrawLine (new Vector3 (map.gaps[i].maxX, -1), new Vector3 (map.gaps[i].maxX, 1));
            }

            Gizmos.color = Color.green;

            Gizmos.DrawLine (new Vector3 (map.buildingsMinX, -3), new Vector3 (map.buildingsMinX, 3));
            Gizmos.DrawLine (new Vector3 (map.buildingsMaxX, -3), new Vector3 (map.buildingsMaxX, 3));
        }
    }
}
