﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSlideIn : MonoBehaviour
{
    public int pixelSnap = 4;
    public float movementSpeed = 76;
    public Game game;

    public float hiddenPosition = 0;
    public float outPosition = 0;

    public bool invert;

    private void Update ()
    {
        Vector3 position = transform.localPosition;

        if (invert)
        {
            if (game.state == GameState.Playing)
            {
                if (position.y > outPosition) position.y -= Time.deltaTime * movementSpeed;
                else position.y = outPosition;
            }
            else
            {
                if (position.y < hiddenPosition) position.y += Time.deltaTime * movementSpeed;
                else position.y = hiddenPosition;
            }
        }
        else
        {
            if (game.state == GameState.Playing)
            {
                if (position.y < hiddenPosition) position.y += Time.deltaTime * movementSpeed;
                else position.y = hiddenPosition;
            }
            else
            {
                if (position.y > outPosition) position.y -= Time.deltaTime * movementSpeed;
                else position.y = outPosition;
            }
        }

        position.x = Mathf.FloorToInt (position.x * pixelSnap) / (float)pixelSnap;
        transform.localPosition = position;
    }
}
