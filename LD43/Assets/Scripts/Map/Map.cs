﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map
{
    public delegate void StartCoroutine (IEnumerator enumerator);

    public readonly StartCoroutine startCoroutine;
    public readonly float wealthFactor;
    public readonly int maxWealthLevel;
    public readonly float basePowerGain;
    public readonly float baseSacrificeCost;
    public readonly float pixelSize;

    public uint peopleWithNoHouse;
    public uint followersWhoCantSacrifice;
    public uint currentFollowers { get; private set; }
    public uint maxFollowers { get; private set; }
    public uint currentGodPower { get; private set; }

    private List<Building> buildings = new List<Building> ();
    private List<Building> unfinishedBuildings = new List<Building> ();
    private Queue<Building> destroyedBuildings = new Queue<Building> ();
    public List<BuildingGap> gaps = new List<BuildingGap> ();
    public float buildingsMinX = 0;
    public float buildingsMaxX = 0;

    private List<Person> people = new List<Person> ();
    private Queue<Person> deadPeople = new Queue<Person> ();
    private readonly float[] wealthMinimums;

    public int sacrificeBuildingCount { get; private set; }
    public int spareSacrificeBuildingCount { get; private set; }
    public int freeBuildingSpaced;

    public bool paused = false;

    public int PeopleCount
    {
        get
        {
            return people.Count;
        }
    }

    public IEnumerable<Building> Buildings
    {
        get
        {
            for (int i = 0; i < buildings.Count; i++) yield return buildings[i];
        }
    }

    public IEnumerable<Person> People
    {
        get
        {
            for (int i = 0; i < people.Count; i++) yield return people[i];
        }
    }

    private float slowPowerTrickleTimer;

    public Map (float wealthFactor, int maxWealthLevels, uint initialGodPower, float basePowerGain, float baseSacrificeCost, float pixelSize, StartCoroutine startCoroutine)
    {
        this.startCoroutine = startCoroutine;
        this.wealthFactor = wealthFactor;
        this.maxWealthLevel = maxWealthLevels;
        this.basePowerGain = basePowerGain;
        this.baseSacrificeCost = baseSacrificeCost;
        this.pixelSize = pixelSize;
        this.currentGodPower = initialGodPower;

        sacrificeBuildingCount = 0;
        slowPowerTrickleTimer = 15;

        wealthMinimums = new float[maxWealthLevels];
        for (int i = 0; i < wealthMinimums.Length; i++) wealthMinimums[i] = CalculateWealth (i / (float)maxWealthLevels);
    }

    public void Update (float deltaTime)
    {
        if (slowPowerTrickleTimer > 0) slowPowerTrickleTimer -= deltaTime;
        else
        {
            slowPowerTrickleTimer = 15;
            currentGodPower++;
        }

        Person person;
        while (deadPeople.Count > 0)
        {
            person = deadPeople.Dequeue ();
            if (people.Contains (person)) people.Remove (person);
        }

        if (destroyedBuildings.Count > 0)
        {
            Building building;
            while (destroyedBuildings.Count > 0)
            {
                building = destroyedBuildings.Dequeue ();
                if (buildings.Contains (building)) buildings.Remove (building);
                if (building is Building_Sacrifice) sacrificeBuildingCount--;
            }

            if (buildings.Count == 0)
            {
                buildingsMaxX = 0;
                buildingsMinX = 0;
            }
            else
            {
                // We can keep the buildings sorted, so its is a single pass operation to add any gaps back
                buildingsMinX = buildings[0].minX;
                buildingsMaxX = buildings[buildings.Count - 1].maxX;

                gaps.Clear ();

                if (buildings.Count > 1)
                {
                    for (int i = 0; i < buildings.Count - 1; i++)
                    {
                        addGapIfBigEnough (buildings[i].maxX, buildings[i + 1].minX);
                    }
                }

                gaps.Sort ((a, b) => { return (int)Mathf.Sign (a.originDistance - b.originDistance); });
            }
        }

        unfinishedBuildings.Clear ();

        spareSacrificeBuildingCount = 0;
        freeBuildingSpaced = 0;

        for (int i = 0; i < buildings.Count; i++)
        {
            buildings[i].Update (deltaTime);
            if (buildings[i].builtState == Building.BuiltState.Started || buildings[i].builtState == Building.BuiltState.Partial) unfinishedBuildings.Add (buildings[i]);
            else if (buildings[i].builtState == Building.BuiltState.Completed)
            {
                if (buildings[i] is Building_Sacrifice && !buildings[i].IsInUse) spareSacrificeBuildingCount++;
            }
        }

        currentFollowers = 0;
        followersWhoCantSacrifice = 0;
        peopleWithNoHouse = 0;

        for (int i = 0; i < people.Count; i++)
        {
            people[i].Update (deltaTime);
            if (people[i].IsFollower) currentFollowers++;
        }

        if (currentFollowers > maxFollowers) maxFollowers = currentFollowers;
    }

    public IEnumerable<Building> BuildingsInRange (float position, float range)
    {
        for (int i = 0; i < buildings.Count; i++)
        {
            if ((buildings[i].minX > position - range && buildings[i].minX < position + range)
                || (buildings[i].maxX > position - range && buildings[i].maxX < position + range))
            {
                yield return buildings[i];
            }
        }
    }

    public IEnumerable<Person> PeopleInRange (float position, float range)
    {
        for (int i = 0; i < people.Count; i++)
        {
            if (Mathf.Abs (people[i].position - position) < range) yield return people[i];
        }
    }

    public float CalculateWealthCoefficient (float wealth)
    {
        return 1.0f - Mathf.Pow (wealthFactor, -wealth);
    }

    public float CalculateWealth (float coefficient)
    {
        return -Mathf.Log (1.0f - coefficient, wealthFactor);
    }

    public int GetWealthLevel (float wealth)
    {
        for (int i = wealthMinimums.Length - 1; i >= 0; i--)
        {
            if (wealth > wealthMinimums[i]) return i;
        }

        return 0;
    }

    public float GetMinWealth (int wealthLevel)
    {
        return wealthMinimums[Mathf.Clamp (wealthLevel, 0, maxWealthLevel - 1)];
    }

    public float GetClosestBuildingLocation (float width, float from, float spacingFactor = 1.05f)
    {
        width *= spacingFactor;

        for (int i = 0; i < gaps.Count; i++)
        {
            if (width <= gaps[i].width)
            {
                if (Mathf.Abs (buildingsMaxX - from) < gaps[i].originDistance || Mathf.Abs (buildingsMinX - from) < gaps[i].originDistance)
                {
                    if (Mathf.Abs (buildingsMaxX - from) < Mathf.Abs (buildingsMinX - from)) return buildingsMaxX + (width / 2f);
                    else return buildingsMinX - (width / 2f);
                }
                else
                {
                    if (gaps[i].position > 0) return gaps[i].minX + (width / 2f);
                    else return gaps[i].maxX - (width / 2f);
                }
            }
        }

        if (Mathf.Abs(buildingsMaxX - from) < Mathf.Abs(buildingsMinX - from)) return buildingsMaxX + (width / 2f);
        else return buildingsMinX - (width / 2f);
    }

    //public Building AddBuilding (string name, float position)
    //{
    //    BuildingType type = Building.GetType (name);
    //    if (type != null)
    //    {
    //        Building building = type.CreateBuildingInstance (this, position);
    //        if (tryAddBuilding (building)) return building;
    //    }

    //    return null;
    //}

    public Building AddBuilding (string name)
    {
        BuildingType type = Building.GetType (name);
        if (type != null)
        {
            Building building = type.CreateBuildingInstance (this, GetClosestBuildingLocation (type.width, 0));
            if (tryAddBuilding (building)) return building;
        }

        return null;
    }

    public Building AddBuilding (BuildingType type, float position)
    {
        if (type != null)
        {
            Building building = type.CreateBuildingInstance (this, position);
            if (tryAddBuilding (building)) return building;
        }

        return null;
    }

    //public Building AddBuilding (BuildingType type)
    //{
    //    if (type != null)
    //    {
    //        Building building = type.CreateBuildingInstance (this, GetClosestBuildingLocation (type.width, 0));
    //        if (tryAddBuilding (building)) return building;
    //    }

    //    return null;
    //}

    private bool tryAddBuilding (Building building)
    {
        for (int i = 0; i < buildings.Count; i++)
        {
            if (buildings[i].DoesOverlap (building)) return false;
        }

        bool added = false;
        for (int i = 0; i < buildings.Count; i++)
        {
            if (building.position < buildings[i].position)
            {
                buildings.Insert (i, building);
                added = true;
                break;
            }
        }

        if (!added) buildings.Add (building);
        if (building is Building_Sacrifice) sacrificeBuildingCount++;

        if (buildings.Count == 1)
        {
            buildingsMaxX = building.maxX;
            buildingsMinX = building.minX;
        }
        else
        {
            // We now need to fix the gaps;
            if (building.maxX > buildingsMaxX)
            {
                if (building.minX > buildingsMaxX) addGapIfBigEnough (buildingsMaxX, building.minX);

                buildingsMaxX = building.maxX;
            }
            else
            {
                for (int i = gaps.Count - 1; i >= 0; i--)
                {
                    if (gaps[i].Contains (building))
                    {
                        addGapIfBigEnough (building.minX, gaps[i].minX);
                        addGapIfBigEnough (building.maxX, gaps[i].maxX);

                        gaps.RemoveAt (i);
                        break;
                    }
                }
            }

            if (building.minX < buildingsMinX)
            {
                if (building.maxX < buildingsMinX) addGapIfBigEnough (building.maxX, buildingsMinX);

                buildingsMinX = building.minX;
            }
            else
            {
                for (int i = gaps.Count - 1; i >= 0; i--)
                {
                    if (gaps[i].Contains (building))
                    {
                        addGapIfBigEnough (building.minX, gaps[i].minX);
                        addGapIfBigEnough (building.maxX, gaps[i].maxX);

                        gaps.RemoveAt (i);
                        break;
                    }
                }
            }

            gaps.Sort ((a, b) => { return (int)Mathf.Sign (a.originDistance - b.originDistance); });
        }

        return true;
    }

    public void RemoveBuilding (Building building)
    {
        destroyedBuildings.Enqueue (building);
    }

    public Person AddPerson (float x, int startingWealthLevel)
    {
        Person person = new Person (this, x, new Vector2 (Random.Range (0f, 1f), Random.Range (0f, 1f)), wealthMinimums[startingWealthLevel] + 0.01f);
        people.Add (person);
        return person;
    }

    public void RemovePerson (Person person)
    {
        deadPeople.Enqueue (person);
    }

    public Person RandomPerson ()
    {
        if (people.Count == 0) return null;
        return people[Random.Range (0, people.Count)];
    }

    public void TryUsePower (GodPower power, float position)
    {
        if (power.cost <= currentGodPower && currentFollowers >= power.minFollowers)
        {
            power.PerformAction (
                this,
                position,
                (used) =>
                {
                    if (used) currentGodPower -= power.cost;
                });
        }
        else
        {
            Sound.PlayFX ("InvalidPower");
        }
    }

    public void FinishSacrifice (Person person, Building_Sacrifice building)
    {
        float wealthPowerModifier = 1.0f;

        switch (person.wealthLevel)
        {
            case 0:
                wealthPowerModifier = 1.0f;
                break;
            case 1:
                wealthPowerModifier = 2.0f;
                break;
            case 2:
                wealthPowerModifier = 5.0f;
                break;
            case 3:
                wealthPowerModifier = 15.0f;
                break;
            default:
                Debug.LogWarning ("Unknown wealth level - " + person.wealthLevel);
                break;
        }

        person.AddWealth (-baseSacrificeCost * wealthPowerModifier);
        currentGodPower += (uint)Mathf.Ceil (basePowerGain * wealthPowerModifier * building.powerGianModifier);
    }

    public Building GetIncompleteBuilding (Person person)
    {
        if (unfinishedBuildings.Count == 0) return null;

        Building currentBest = null;
        Building tmp;
        float bestWeight = 0;
        float tmpWeight;
        for (int i = 0; i < unfinishedBuildings.Count; i++)
        {
            tmp = unfinishedBuildings[i];
            if (person.wealthLevel >= tmp.minimumWealthLevel && !tmp.IsInUse)
            {
                tmpWeight = getChoseBuildingWeight (tmp.minimumWealthLevel, Mathf.Abs (tmp.GetClosestEntrance (person) - person.position));
                if (currentBest == null || tmpWeight > bestWeight)
                {
                    bestWeight = tmpWeight;
                    currentBest = tmp;
                }
            }
        }

        return currentBest;
    }

    public Building_House FindBestHouse (Person person)
    {
        Building_House currentBest = null;
        Building_House tmp;
        float bestWeight = 0;
        float tmpWeight;
        for (int i = 0; i < buildings.Count; i++)
        {
            if (buildings[i].builtState == Building.BuiltState.Completed && buildings[i] is Building_House)
            {
                tmp = (Building_House)buildings[i];

                if (person.wealthLevel >= tmp.minimumWealthLevel && tmp.IsSpace)
                {
                    tmpWeight = getChoseBuildingWeight (tmp.minimumWealthLevel, Mathf.Abs (tmp.GetClosestEntrance (person) - person.position));
                    if (currentBest == null || tmpWeight > bestWeight)
                    {
                        bestWeight = tmpWeight;
                        currentBest = tmp;
                    }
                }
                else
                {
                    if (tmp.currentPerson == person) return null;
                }
            }
        }

        return currentBest;
    }

    public Building_Sacrifice FindBestSacrificeBuilding (Person person)
    {
        Building_Sacrifice currentBest = null;
        Building_Sacrifice tmp;
        float bestWeight = 0;
        float tmpWeight;
        for (int i = 0; i < buildings.Count; i++)
        {
            if (buildings[i].builtState == Building.BuiltState.Completed && buildings[i] is Building_Sacrifice)
            {
                tmp = (Building_Sacrifice)buildings[i];

                if (person.wealthLevel >= tmp.minimumWealthLevel && !tmp.IsInUse)
                {
                    tmpWeight = getChoseBuildingWeight (tmp.minimumWealthLevel, Mathf.Abs (tmp.GetClosestEntrance (person) - person.position));
                    if (currentBest == null || tmpWeight > bestWeight)
                    {
                        bestWeight = tmpWeight;
                        currentBest = tmp;
                    }
                }
                else
                {
                    if (tmp.currentPerson == person) return null;
                }
            }
        }

        return currentBest;
    }

    public int GetBuildintTypeCont<T> () where T : Building
    {
        if (typeof (T) == typeof (Building_Sacrifice)) return sacrificeBuildingCount;

        int count = 0;

        for (int i = 0; i < buildings.Count; i++)
        {
            if (buildings[i] is T) count++;
        }

        return count;
    }

    private float getChoseBuildingWeight (int minimumWealthLevel, float distance)
    {
        return (minimumWealthLevel * 10) - (distance);
    }

    private void addGapIfBigEnough (float left, float right)
    {
        float width = Mathf.Abs (right - left);
        if (width >= Building.minBuildingWidth) gaps.Add (new BuildingGap ((left + right) / 2f, width));
    }

    public void DoBeliefEffect (float position, float range, float baseValue, float decay)
    {
        foreach (Person item in PeopleInRange (position, range))
            item.AddBelief (baseValue * Mathf.Pow (decay, Mathf.Abs (item.position - position) / range));
    }
}
