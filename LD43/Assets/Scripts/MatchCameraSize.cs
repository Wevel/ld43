﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Camera))]
public class MatchCameraSize : MonoBehaviour {

    public Camera target;

    private Camera thisCamera;

    private void Awake ()
    {
        thisCamera = this.GetComponent<Camera> ();
    }

    void Update () {
        if (target != null)
        {
            thisCamera.orthographicSize = target.orthographicSize;
        }
	}
}
