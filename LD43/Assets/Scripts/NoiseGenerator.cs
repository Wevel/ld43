﻿
#if UNITY_EDITOR

using System.IO;
using UnityEngine;
using UnityEditor;

public static class NoiseGenerator
{
    public static Texture2D GenerateNoiseTexture (int size)
    {
        System.Random rnd = new System.Random ();
        Texture2D tex = new Texture2D (size, size);

        Color[] colours = tex.GetPixels ();

        float value;
        for (int i = 0; i < colours.Length; i++)
        {
            value = (float)rnd.NextDouble ();
            colours[i] = new Color (value, value, value, 1);
        }

        tex.SetPixels (colours);
        tex.Apply ();

        return tex;
    }

    [MenuItem ("Tools/Generate Noise Texture")]
    private static void createNewNoiseTexture ()
    {
        string path = EditorUtility.SaveFilePanelInProject ("Save As", "Noise", "png", "");
        if (path != "")
        {
            File.WriteAllBytes (path, GenerateNoiseTexture (1024).EncodeToPNG ());
        }
    }
}

#endif