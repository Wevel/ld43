﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParticleSpawnData
{
    public Color colourA;
    public Color colourB;
    public float randomDirectionAmount = 1;
    public float minTime = 0.5f;
    public float maxTime = 1.0f;
    public bool useGravity = true;

    public void SpawnParticle (Vector2 position, Vector2 velocity)
    {
        PixelParticleSystem.AddParticle (position, velocity + Random.insideUnitCircle * randomDirectionAmount, Color.Lerp (colourA, colourB, Random.value), Random.Range (minTime, maxTime), useGravity);
    }
}
