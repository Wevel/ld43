﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelParticle
{
    public Vector2 position;
    public Vector2 velocity;
    public readonly Color colour;
    public float maxTime;
    public float remainingTime;
    public bool useGravity;

    public PixelParticle (Vector2 position, Vector2 velocity, Color colour, float time, bool useGravity)
    {
        this.position = position;
        this.velocity = velocity;
        this.colour = colour;
        this.maxTime = time;
        this.remainingTime = time;
        this.useGravity = useGravity;
    }
}
