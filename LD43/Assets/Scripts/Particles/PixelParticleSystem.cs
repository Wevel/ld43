﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelParticleSystem : MonoBehaviour
{
    private static PixelParticleSystem instance;

    public static void AddParticle (Vector2 position, Vector2 velocity, Color colour, float time = -1, bool useGravity = true)
    {
        if (instance != null) instance.particles.Add (new PixelParticle (position, velocity, colour, time, useGravity));
    }

    private Color clearColour = Color.clear;

    public int pixelsPerUnit = 8;
    public float gravity = -9.8f;

    private List<PixelParticle> particles = new List<PixelParticle> ();

    private int textureWidth;
    private int textureHeight;
    private float bottomLimit;

    public Sprite sprite { get; private set; }
    private Texture2D spriteTexture;
    private Color[] textureColours;
    
    private void Awake ()
    {
        Camera camera = Camera.main;
        textureWidth = Mathf.CeilToInt (camera.orthographicSize * 2f * camera.aspect * pixelsPerUnit) + 2;
        textureHeight = Mathf.CeilToInt (camera.orthographicSize * 2f * pixelsPerUnit) + 2;
        bottomLimit = -camera.orthographicSize * 1.2f;

        if (textureWidth % 2 == 1) textureWidth++;
        if (textureHeight % 2 == 1) textureHeight++;

        spriteTexture = new Texture2D (textureWidth, textureHeight);
        spriteTexture.wrapMode = TextureWrapMode.Clamp;
        spriteTexture.filterMode = FilterMode.Point;
        textureColours = spriteTexture.GetPixels ();

        sprite = Sprite.Create (spriteTexture, new Rect (0, 0, textureWidth, textureHeight), new Vector2 (0.5f, 0.5f), pixelsPerUnit);
    }

    private void OnEnable ()
    {
        instance = this;
    }

    private void OnDisable ()
    {
        instance = null;
    }

    private void OnDestroy ()
    {
        if (sprite != null) Destroy (sprite);
        if (spriteTexture != null) Destroy (spriteTexture);
    }

    private void Update ()
    {
        Camera camera = Camera.main;

        for (int i = 0; i < textureColours.Length; i++) textureColours[i] = clearColour;

        int x, y;
        PixelParticle particle;
        for (int i = particles.Count - 1; i >= 0; i--)
        {
            particle = particles[i];
            if (particle.useGravity) particle.velocity.y += gravity * Time.deltaTime;
            particle.position += particle.velocity * Time.deltaTime;

            if (particle.maxTime > 0)
            {
                if (particle.remainingTime > 0) particle.remainingTime -= Time.deltaTime;
            }

            if (particle.position.y < bottomLimit || particle.remainingTime < 0)
            {
                particles.RemoveAt (i);
            }
            else
            {
                x = Mathf.FloorToInt ((particle.position.x - camera.transform.position.x) * pixelsPerUnit);
                y = Mathf.FloorToInt ((particle.position.y - camera.transform.position.y) * pixelsPerUnit);
                x += textureWidth / 2;
                y += textureHeight / 2;
                if (x >= 0 && x < textureWidth && y >= 0 && y < textureHeight) textureColours[x + (y * textureWidth)] = particle.colour;
            }
        }

        spriteTexture.SetPixels (textureColours);
        spriteTexture.Apply ();
    }
}
