﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person
{
    public const float wanderDistance = 1.0f;
    public const float nonBelieverWealthGainRate = 0.0025f;
    public const float nonBelieverWealthGainRateVariation = 0.2f;
    public const float buildSpeed = 0.1f;

    public const float averageSacrificeWaitDelay = 15f;
    public const float sacrificeWaitVariation = 5f;

    private float getSacrificeWaitTime ()
    {
        return averageSacrificeWaitDelay + Random.Range (-sacrificeWaitVariation, sacrificeWaitVariation) + (5 * wealthLevel);
    }

    private readonly Map map;
    private readonly TaskQueue taskQueue;

    public readonly Vector2 noisePosition;
    
    public float position;
    public float movementSpeed = 1;
    public float beliefDecayDelay = 10f;
    public float beliefDecayRate = 0.01f;
    public float belief { get; private set; }
    public float wealth { get; private set; }
    public float wealthCoefficient { get; private set; }
    public int wealthLevel { get; private set; }
    public bool alive { get; private set; }
    public Building_House house { get; private set; }
    
    public bool hidden = false;

    private float beliefDecayTimer;
    private float sacrificeTimer;

    public bool IsFollower
    {
        get
        {
            return belief > 0.5f;
        }
    }

    public Person (Map map, float position, Vector2 noisePosition, float initialWealth)
    {
        this.map = map;
        this.position = position;
        this.noisePosition = noisePosition;
        this.wealth = 0;

        taskQueue = new TaskQueue (this);
        beliefDecayTimer = 0;
        alive = true;
        sacrificeTimer = averageSacrificeWaitDelay + Random.Range (-sacrificeWaitVariation, sacrificeWaitVariation);

        AddWealth (initialWealth);
    }

    public void Update (float deltaTime)
    {
        if (alive)
        {
            if (house != null)
            {
                if (house.builtState != Building.BuiltState.Completed)
                {
                    house.RemoveOwner (this);
                    house = null;
                }
            }

            if (house == null)
            {
                house = map.FindBestHouse (this);
                if (house == null || !house.TryAddOwner (this)) map.peopleWithNoHouse++;
            }
            else
            {
                Building_House h;
                foreach (Building item in map.Buildings)
                {
                    if (item is Building_House)
                    {
                        h = (Building_House)item;
                        if (wealthLevel >= h.minimumWealthLevel && h.minimumWealthLevel > house.minimumWealthLevel)
                        {
                            house.RemoveOwner (this);
                            house = null;
                            break;
                        }
                    }
                }
            }

            taskQueue.Update (deltaTime);

            if (beliefDecayTimer < beliefDecayDelay)
            {
                beliefDecayTimer += deltaTime;
            }
            else
            {
                beliefDecayTimer = beliefDecayDelay;

                if (belief > 0) belief -= deltaTime * beliefDecayRate;
                else belief = 0;
            }

            if (IsFollower)
            {
                if (sacrificeTimer > 0) sacrificeTimer -= deltaTime;
                else if (!taskQueue.ContainstTask<Task_Sacrifice> ())
                {
                    Building_Sacrifice building = map.FindBestSacrificeBuilding (this);
                    if (building != null)
                    {
                        taskQueue.ClearQueue ();
                        taskQueue.AddTask (new Task_Sacrifice (building));
                        taskQueue.NextTask ();
                    }
                    else
                    {
                        map.followersWhoCantSacrifice++;
                    }
                }
            }
            else
            {
                AddWealth (nonBelieverWealthGainRate * (1f + Random.Range (-nonBelieverWealthGainRateVariation, nonBelieverWealthGainRateVariation)) * deltaTime);
            }
        }
        else
        {
            if (house != null)
            {
                house.RemoveOwner (this);
                house = null;
            }
        }
    }

    public void Kill ()
    {
        alive = false;
        map.RemovePerson (this);

        if (house != null)
        {
            house.RemoveOwner (this);
            house = null;
        }
    }

    public void AddWealth (float amount)
    {
        wealth += amount;
        wealthCoefficient = map.CalculateWealthCoefficient (wealth);
        wealthLevel = map.GetWealthLevel (wealth);
    }

    public void AddBelief (float amount)
    {
        belief = Mathf.Clamp01 (belief + amount);
        beliefDecayTimer = 0;
    }

    public void MakeBeliever ()
    {
        belief = 0.75f;
        beliefDecayTimer = 0;
    }

    public void FinishedTasks ()
    {
        bool failedSacrificeTask = false;

        if (IsFollower && sacrificeTimer < 0)
        {
            Building_Sacrifice building = map.FindBestSacrificeBuilding (this);
            if (building != null) taskQueue.AddTask (new Task_Sacrifice (building));
            else failedSacrificeTask = true;
        }
        else
        {
            failedSacrificeTask = true;
        }

        if (failedSacrificeTask)
        {
            Building toFinishBuilding = map.GetIncompleteBuilding (this);
            if (toFinishBuilding != null && Mathf.Abs (toFinishBuilding.position - position) < 10f)
            {
                taskQueue.AddTask (new Task_DoBuildingWork (toFinishBuilding));
            }
            else
            {
                BuildingType type = getTypeToBuild ();
                if (type != null)
                {
                    float targetPosition = map.GetClosestBuildingLocation (type.width, position * 0.25f);
                    if ((type.followerBuilding && Mathf.Abs (targetPosition - position) < 10f)
                        || (!type.followerBuilding && Mathf.Abs (targetPosition - position) < 15f))
                    {
                        float requiredWealth = type.upfrontBuildCost;
                        if (type.requiredWealthLevel == 0) requiredWealth += map.GetMinWealth (type.requiredWealthLevel + 1) * 0.5f;
                        else requiredWealth += map.GetMinWealth (type.requiredWealthLevel);

                        if (wealth >= requiredWealth) taskQueue.AddTask (new Task_StartBuilding (type, targetPosition));
                        else randomwWander ();
                    }
                    else
                    {
                        randomwWander ();
                    }
                }
                else
                {
                    randomwWander ();
                }
            }
        }
    }

    private void randomwWander ()
    {
        float variation = Random.Range (-wanderDistance, wanderDistance);

        if (position > 0)
        {
            if (position > map.buildingsMaxX) variation -= 0.5f * wanderDistance * (position - map.buildingsMaxX) / map.buildingsMaxX;
        }
        else
        {
            if (position < map.buildingsMinX) variation -= 0.5f * wanderDistance * (map.buildingsMinX - position) / map.buildingsMinX;
        }

        taskQueue.AddTask (new Task_Move (position + variation, 0.5f));
    }

    public bool HasFinishedSacrifice ()
    {
        return !hidden && sacrificeTimer > 0;
    }

    public void FinishSacrifice ()
    {
        sacrificeTimer = getSacrificeWaitTime ();
    }

    public void TryPlaceBuilding (BuildingType type, float position)
    {
        Building b = map.AddBuilding (type, position);
        if (b != null) AddWealth (-type.upfrontBuildCost);
    }

    private BuildingType getTypeToBuild ()
    {
        BuildingType bestType = null;
        float bestWeight = 0;
        float tmpWeight;
        foreach (BuildingType item in Building.BuildingTypes)
        {
            tmpWeight = item.GetBuildDecisionWeight (map, this);
            if (tmpWeight > 0)
            {
                if (bestType == null || tmpWeight >= bestWeight)
                {
                    bestWeight = tmpWeight;
                    bestType = item;
                }
            }
        }

        return bestType;
    }
}
