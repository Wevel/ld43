﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskQueue
{
    public readonly Person owner;
    public bool interrupted { get; private set; }

    private Queue<Task> taskQueue = new Queue<Task> ();
    public Task currentTask { get; private set; }

    public TaskQueue (Person owner)
    {
        this.owner = owner;
    }

    public void Update (float deltaTime)
    {
        interrupted = false;

        if (currentTask != null) currentTask.Update (this, deltaTime);
        else NextTask ();
    }

    public void NextTask ()
    {
        if (taskQueue.Count > 0)
        {
            currentTask = taskQueue.Dequeue ();
            currentTask.Start (this);
        }
        else
        {
            owner.FinishedTasks ();
        }
    }

    public void InteruptTask (params Task[] newTasks)
    {
        interrupted = true;

        Queue<Task> newTaskQueue = new Queue<Task> (newTasks);

        if (currentTask != null)
        {
            currentTask.Pause (this);
            newTaskQueue.Enqueue (currentTask);
        }

        while (taskQueue.Count > 0) newTaskQueue.Enqueue (taskQueue.Dequeue ());

        taskQueue = newTaskQueue;
        NextTask ();
    }

    public void AddTask (Task task)
    {
        taskQueue.Enqueue (task);
    }

    public void ClearQueue ()
    {
        currentTask = null;
        taskQueue.Clear ();
        interrupted = true;
    }

    internal bool ContainstTask<T> () where T : Task
    {
        foreach (Task item in taskQueue)
        {
            if (item is T) return true;
        }

        return false;
    }
}
