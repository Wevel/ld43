﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Task
{
    public readonly string name;

    private bool started = false;

    public Task (string name)
    {
        this.name = name;
    }

    public void Start (TaskQueue taskQueue)
    {
        if (!started)
        {
            started = true;
            OnStart (taskQueue);
        }
    }

    public void Pause (TaskQueue taskQueue)
    {
        if (started)
        {
            started = false;
            OnPause (taskQueue);
        }
    }

    public void Update (TaskQueue taskQueue, float deltaTime)
    {
        if (!started) Start (taskQueue);

        if (OnUpdate (taskQueue, deltaTime))
        {
            started = false;

            OnFinish (taskQueue);

            if (!taskQueue.interrupted) taskQueue.NextTask ();
        }
    }

    protected virtual void OnStart (TaskQueue taskQueue) { }
    protected virtual void OnPause (TaskQueue taskQueue) { }
    protected virtual void OnFinish (TaskQueue taskQueue) { }
    protected virtual bool OnUpdate (TaskQueue taskQueue, float deltaTime) { return true; }
}


public class Task_Move : Task
{
    public float target;
    public float speed;

    public Task_Move (float target, float speed = 1) : base ("Move")
    {
        this.target = target;
        this.speed = speed;
    }

    protected override bool OnUpdate (TaskQueue taskQueue, float deltaTime)
    {
        float distance = Mathf.Abs (target - taskQueue.owner.position);
        float moveDelta = deltaTime * taskQueue.owner.movementSpeed * Mathf.Clamp01 (speed);
        if (distance < moveDelta)
        {
            taskQueue.owner.position = target;
            return true;
        }
        else
        {
            taskQueue.owner.position = Mathf.Lerp (taskQueue.owner.position, target, moveDelta / distance);
            return false;
        }

    }
}


public class Task_Sacrifice : Task
{
    private Building_Sacrifice currentTarget;

    public Task_Sacrifice (Building_Sacrifice target) : base ("Sacrifice")
    {
        currentTarget = target;
    }

    protected override void OnStart (TaskQueue taskQueue)
    {
        if (currentTarget == null)
        {
            taskQueue.NextTask ();
            return;
        }

        float entrance = currentTarget.GetClosestEntrance (taskQueue.owner);
        if (Mathf.Abs (entrance - taskQueue.owner.position) > 0.125f) taskQueue.InteruptTask (new Task_Move (entrance));
        else if (!currentTarget.TryUseBuilding (taskQueue.owner)) taskQueue.NextTask ();
    }

    protected override void OnPause (TaskQueue taskQueue)
    {
        if (taskQueue.owner.hidden) currentTarget.FinishUseBuilding (taskQueue.owner);
    }

    protected override bool OnUpdate (TaskQueue taskQueue, float deltaTime)
    {
        return taskQueue.owner.HasFinishedSacrifice ();
    }
}

public class Task_StartBuilding : Task
{
    private readonly BuildingType type;
    private readonly float position;

    public Task_StartBuilding (BuildingType type, float position) : base ("Start Build")
    {
        this.type = type;
        this.position = position;
    }

    protected override void OnStart (TaskQueue taskQueue)
    {
        if (type == null)
        {
            taskQueue.NextTask ();
            return;
        }

        if (Mathf.Abs (position - taskQueue.owner.position) > 0.125f) taskQueue.InteruptTask (new Task_Move (position));
    }

    protected override bool OnUpdate (TaskQueue taskQueue, float deltaTime)
    {
        if (Mathf.Abs (position - taskQueue.owner.position) > 0.125f)
        {
            taskQueue.InteruptTask (new Task_Move (position));
            return false;
        }
        else
        {
            return true;
        }
    }

    protected override void OnFinish (TaskQueue taskQueue)
    {
        taskQueue.owner.TryPlaceBuilding (type, position);
    }
}

public class Task_DoBuildingWork : Task
{
    private readonly Building building;

    public Task_DoBuildingWork (Building building) : base ("Build")
    {
        this.building = building;
    }

    protected override void OnStart (TaskQueue taskQueue)
    {
        if (building == null || building.builtState == Building.BuiltState.Completed || building.builtState == Building.BuiltState.Destroyed)
        {
            taskQueue.NextTask ();
            return;
        }

        if (Mathf.Abs (building.position - taskQueue.owner.position) > 0.125f) taskQueue.InteruptTask (new Task_Move (building.position));
        else if (!building.TryUseBuilding (taskQueue.owner)) taskQueue.NextTask ();
    }

    protected override void OnPause (TaskQueue taskQueue)
    {
        if (taskQueue.owner.hidden) building.FinishUseBuilding (taskQueue.owner);
    }

    protected override bool OnUpdate (TaskQueue taskQueue, float deltaTime)
    {
        if (building == null || building.builtState == Building.BuiltState.Completed || building.builtState == Building.BuiltState.Destroyed)
        {
            taskQueue.NextTask ();
            return false;
        }

        if (Mathf.Abs (building.position - taskQueue.owner.position) > 0.125f)
        {
            taskQueue.InteruptTask (new Task_Move (building.position));
            return false;
        }
        else
        {
            building.buildPercent += deltaTime * Person.buildSpeed / (1.0f + building.minimumWealthLevel);
            return building.buildPercent >= 1;
        }
    }

    protected override void OnFinish (TaskQueue taskQueue)
    {
        if (taskQueue.owner.hidden) building.FinishUseBuilding (taskQueue.owner);
        building.FinishBuildWork (taskQueue.owner);
    }
}