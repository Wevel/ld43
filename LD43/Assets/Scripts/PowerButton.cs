﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerButton : MonoBehaviour
{
    public int buttonIndex = 0;
    public int pixelSnap = 4;
    public float movementSpeed = 76;
    public Game game;

    public float hiddenPosition = -76;
    public float selectedPosition = -10;
    public float outPosition = 0;

    private void Update ()
    {
        Vector3 position = transform.position;

        if (game.state == GameState.Playing && game.map.currentFollowers >= game.godPowers[buttonIndex].minFollowers)
        {
            if (game.currentgodPowerIndex == buttonIndex)
            {
                if (Mathf.Abs (position.x - selectedPosition) > Time.deltaTime * movementSpeed)
                    position.x = Mathf.Lerp (position.x, selectedPosition, Time.deltaTime * movementSpeed / Mathf.Abs (position.x - selectedPosition));
                else
                    position.x = selectedPosition;
            }
            else
            {
                if (position.x < outPosition) position.x += Time.deltaTime * movementSpeed;
                else position.x = outPosition;
            }
        }
        else
        {
            if (position.x > hiddenPosition) position.x -= Time.deltaTime * movementSpeed;
            else position.x = hiddenPosition;
        }

        position.x = Mathf.FloorToInt (position.x * pixelSnap) / (float)pixelSnap;
        transform.position = position;
    }
}
