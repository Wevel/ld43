﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void FinishGodPowerAction (bool used);

public abstract class GodPower
{
    public readonly string name;
    public readonly uint cost;
    public readonly float baseAwe;
    public readonly float aweRange;
    public readonly float aweDecay;
    public readonly float hitRange;
    public readonly Color regionColour;
    public readonly int minFollowers;

    protected GodPower (string name, uint cost, float baseAwe, float aweRange, float aweDecay, float hitRange, Color regionColour, int minFollowers)
    {
        this.name = name;
        this.cost = cost;
        this.baseAwe = baseAwe;
        this.aweRange = aweRange;
        this.aweDecay = aweDecay;
        this.hitRange = hitRange;
        this.regionColour = regionColour;
        this.minFollowers = minFollowers;
    }

    public void PerformAction (Map map, float position, FinishGodPowerAction onComplete)
    {
        map.startCoroutine (IPerformAction (map, position, onComplete));
    }

    protected abstract IEnumerator IPerformAction (Map map, float position, FinishGodPowerAction onComplete);
}

public class GodPower_Smite : GodPower
{
    private readonly ParticleSpawnData particleSpawnData;

    public GodPower_Smite (uint cost, float baseAwe, float aweRange, float aweDecay, float hitRange, Color regionColour, ParticleSpawnData particleSpawnData, int minFollowers)
        : base ("Smite", cost, baseAwe, aweRange, aweDecay, hitRange, regionColour, minFollowers)
    {
        this.particleSpawnData = particleSpawnData;
    }

    protected override IEnumerator IPerformAction (Map map, float position, FinishGodPowerAction onComplete)
    {
        Person closest = null;
        foreach (Person item in map.PeopleInRange (position, hitRange))
        {
            if (!item.hidden)
            {
                if (closest == null || Mathf.Abs (item.position - position) < Mathf.Abs (closest.position - position)) closest = item;
            }
        }

        if (closest != null)
        {
            onComplete (true);
            yield return null;

            Sound.PlayFX ("Smite");

            float angle = 0.4f;
            if (Random.value < 0.5f) angle = -angle;
            float scale = Random.Range (1f, 2f);

            for (float y = 0; y < 10; y += 0.1f)
                particleSpawnData.SpawnParticle (new Vector2 (position + getCombinedlightningOffset (y, angle, scale), y), Vector2.zero);

            closest.Kill ();

            map.DoBeliefEffect (position, aweRange + hitRange, baseAwe, aweDecay);
        }
        else
        {
            onComplete (false);
            Sound.PlayFX ("InvalidPower");
        }
    }

    private static float getCombinedlightningOffset (float y, float angle, float scale)
    {
        const float c = 0.5f;
        const float d = 0.5f;

        return (y * angle) + ((getlightningOffset (y * scale, c, d) + getlightningOffset (y, c, d * 2)) * Mathf.Log (y + 1));
    }

    private static float getlightningOffset (float y, float c, float d)
    {
        int fy = Mathf.FloorToInt (d * y);
        int mod = fy % 2;
        return ((c * ((2 * mod) - 1)) * ((d * y) - fy)) - (c * mod);
    }
}

public class GodPower_FireBall : GodPower
{
    public readonly Vector2 startOffset;
    public readonly float hitDelay;
    public ParticleSpawnData particleSpawnData;

    public GodPower_FireBall (uint cost, float baseAwe, float aweRange, float aweDecay, float hitRange, Vector2 startOffset, float hitDelay, Color regionColour, ParticleSpawnData particleSpawnData, int minFollowers) 
        : base ("Fire Ball", cost, baseAwe, aweRange, aweDecay, hitRange, regionColour, minFollowers)
    {
        this.startOffset = startOffset;
        this.hitDelay = hitDelay;
        this.particleSpawnData = particleSpawnData;
    }
    
    protected override IEnumerator IPerformAction (Map map, float position, FinishGodPowerAction onComplete)
    {
        onComplete (true);
        float t = 0;
        Vector2 target = new Vector2 (position, 0);
        Vector2 start = new Vector2 (position, startOffset.y);
        if (Random.value < 0.5f) start.x += startOffset.x;
        else start.x -= startOffset.x;
        Debug.DrawLine (start, target, Color.red, hitDelay);
        Vector2 currentPosition;

        Sound.PlayFX ("Fireball");

        while (t < hitDelay)
        {
            currentPosition = Vector2.Lerp (start, target, t / hitDelay);

            for (int i = 0; i < 10; i++)
            {
                particleSpawnData.SpawnParticle (currentPosition + (Random.insideUnitCircle * hitRange / 2f), Vector2.zero);
                particleSpawnData.SpawnParticle (currentPosition + (Random.insideUnitCircle * hitRange / 4f), Vector2.zero);
            }

            t += Time.deltaTime;
            yield return null;
        }

        Sound.PlayFX ("FireballExplode");

        Vector2 offset, velocity;
        for (int i = 0; i < 50; i++)
        {
            offset = Random.insideUnitCircle * hitRange;
            offset.y = Mathf.Abs (offset.y) + 0.5f;
            velocity = Random.insideUnitCircle * 6;
            velocity.y = Mathf.Abs (velocity.y);
            particleSpawnData.SpawnParticle (target + offset, velocity);
        }

        foreach (Person item in map.PeopleInRange (position, hitRange))
        {
            if (!item.hidden) item.Kill ();
        }

        map.DoBeliefEffect (position, aweRange + hitRange, baseAwe, aweDecay);
    }
}

public class GodPower_EarthQuake : GodPower
{
    public readonly float duration;
    public readonly ParticleSpawnData particleSpawnData;

    public GodPower_EarthQuake (uint cost, float baseAwe, float aweRange, float aweDecay, float hitRange, float duration, Color regionColour, ParticleSpawnData particleSpawnData, int minFollowers)
        : base ("Earthquake", cost, baseAwe, aweRange, aweDecay, hitRange, regionColour, minFollowers)
    {
        this.duration = duration;
        this.particleSpawnData = particleSpawnData;
    }

    protected override IEnumerator IPerformAction (Map map, float position, FinishGodPowerAction onComplete)
    {
        Debug.Break ();
        onComplete (true);

        List<Building> inRange = new List<Building> ();
        foreach (Building item in map.BuildingsInRange (position, hitRange)) inRange.Add (item);

        Sound.PlayFX ("Earthquake");
        
        if (inRange.Count > 0)
        {
            float destroySeperation = 1f / inRange.Count;
            int i = 0;
            float t = 0;
            int index;
            while (t < duration)
            {
                if (t > destroySeperation * (i + 1) && inRange.Count > 0)
                {
                    index = Random.Range (0, inRange.Count);
                    inRange[index].Destroy ();
                    spawnBuildingParticles (inRange[index]);
                    inRange.RemoveAt (index);
                    i++;
                }

                spawnFrameParticles (position);

                t += Time.deltaTime;
                yield return null;
            }

            if (inRange.Count > 0)
            {
                for (int a = 0; a < inRange.Count; a++)
                {
                    inRange[a].Destroy ();
                    spawnBuildingParticles (inRange[a]);
                }
            }
        }
        else
        {
            float t = 0;
            while (t < duration)
            {
                spawnFrameParticles (position);

                t += Time.deltaTime;
                yield return null;
            }
        }

        map.DoBeliefEffect (position, aweRange + hitRange, baseAwe, aweDecay);
    }

    private void spawnBuildingParticles (Building b)
    {
        if (b.display.backSprite != null) spawnSpriteParticles (b.position, b.verticalOffset, b.display.backSpriteDrawData);
        if (b.display.frontSprite != null) spawnSpriteParticles (b.position, b.verticalOffset, b.display.frontSpriteDrawData);
    }

    private void spawnSpriteParticles (float x, float y, MapRenderer.SpriteDrawData sprite)
    {
        Rect r = sprite.sprite.rect;
        Color c;
        for (int ix = 0; ix < r.width; ix++)
        {
            for (int iy = 0; iy < r.height; iy++)
            {
                c = sprite.sprite.texture.GetPixel ((int)r.xMin + ix, (int)r.yMin + iy);
                if (c.a > 0.9f)
                {
                    PixelParticleSystem.AddParticle (
                        new Vector2 (x + Mathf.Lerp (sprite.xMin, sprite.xMax, (ix + 0.5f) / r.width), y + Mathf.Lerp (sprite.yMin, sprite.yMax, (iy + 0.5f) / r.height)),
                        new Vector2 (0, 2) + Random.insideUnitCircle * particleSpawnData.randomDirectionAmount,
                        c,
                        Random.Range (particleSpawnData.minTime, particleSpawnData.maxTime));
                }
            }
        }
    }

    private void spawnFrameParticles (float position)
    {
        for (int i = 0; i < 2; i++)
            particleSpawnData.SpawnParticle (new Vector2 (position + Random.Range (-hitRange / 2f * 1.5f, hitRange / 2f * 1.5f), Random.Range (-0.25f, 0.125f)), new Vector2 (0, 2));
    }
}

public class GodPower_Bless : GodPower
{
    private readonly ParticleSpawnData particleSpawnData;
    private readonly float beliefAmount;

    public GodPower_Bless (uint cost, float baseAwe, float aweRange, float aweDecay, float hitRange, float beliefAmount, Color regionColour, ParticleSpawnData particleSpawnData, int minFollowers)
        : base ("Bless", cost, baseAwe, aweRange, aweDecay, hitRange, regionColour, minFollowers)
    {
        this.beliefAmount = beliefAmount;
        this.particleSpawnData = particleSpawnData;
    }

    protected override IEnumerator IPerformAction (Map map, float position, FinishGodPowerAction onComplete)
    {
        List<Person> inRange = new List<Person> ();
        foreach (Person item in map.PeopleInRange (position, hitRange))
        {
            if (!item.IsFollower && !item.hidden)
            {
                inRange.Add (item);
            }
        }

        if (inRange.Count > 0)
        {
            onComplete (true);

            yield return null;

            Sound.PlayFX ("Bless");

            for (int i = 0; i < 25; i++)
                particleSpawnData.SpawnParticle (new Vector2 (position + Random.Range (-hitRange, hitRange), 0.5f), Vector2.zero);

            for (int i = 0; i < inRange.Count; i++)
            {
                if (inRange[i].belief < 0.75f) inRange[i].AddBelief (Mathf.Min (beliefAmount / inRange.Count, 0.75f - inRange[i].belief));
            }

            map.DoBeliefEffect (position, aweRange + hitRange, baseAwe, aweDecay);
        }
        else
        {
            onComplete (false);
            Sound.PlayFX ("InvalidPower");
        }
    }
}

public class GodPower_BestowGift : GodPower
{
    private readonly ParticleSpawnData particleSpawnData;

    public GodPower_BestowGift (uint cost, float baseAwe, float aweRange, float aweDecay, float hitRange, Color regionColour, ParticleSpawnData particleSpawnData, int minFollowers)
        : base ("Bestow Gift", cost, baseAwe, aweRange, aweDecay, hitRange, regionColour, minFollowers)
    {
        this.particleSpawnData = particleSpawnData;
    }

    protected override IEnumerator IPerformAction (Map map, float position, FinishGodPowerAction onComplete)
    {
        Person closest = null;
        foreach (Person item in map.PeopleInRange (position, hitRange))
        {
            if (item.wealthLevel < map.maxWealthLevel - 1 && !item.hidden)
            {
                if (closest == null || Mathf.Abs (item.position - position) < Mathf.Abs (closest.position - position)) closest = item;
            }
        }

        if (closest != null)
        {
            onComplete (true);
            yield return null;

            Sound.PlayFX ("Gift");

            for (int i = 0; i < 10; i++)
                particleSpawnData.SpawnParticle (new Vector2 (position, 2f), Vector2.zero);

            closest.AddWealth (map.GetMinWealth (closest.wealthLevel + 1) - map.GetMinWealth (closest.wealthLevel));

            map.DoBeliefEffect (position, aweRange + hitRange, baseAwe, aweDecay);
        }
        else
        {
            onComplete (false);
            Sound.PlayFX ("InvalidPower");
        }
    }
}