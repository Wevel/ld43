﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionMarker : MonoBehaviour
{
    public GameObject holder;
    public SpriteRenderer leftMarker;
    public SpriteRenderer rightMarker;
    public int pixelsPerUnit = 8;

    private float width;
    private float position;

    public void SetWidth (float width)
    {
        this.width = width;
        Update ();
    }

    public void SetColour (Color colour)
    {
        leftMarker.color = colour;
        rightMarker.color = colour;
    }

    public void SetPosition (float position)
    {
        this.position = position;
        Update ();
    }

    public void SetActive (bool active)
    {
        holder.SetActive (active);
    }

    private void Update ()
    {
        leftMarker.transform.localPosition = new Vector3 (Mathf.FloorToInt ((position * pixelsPerUnit) - (width / 2f) * pixelsPerUnit) / (float)pixelsPerUnit, 0);
        rightMarker.transform.localPosition = new Vector3 (Mathf.FloorToInt ((position * pixelsPerUnit) + (width / 2f) * pixelsPerUnit) / (float)pixelsPerUnit, 0);
    }
}
