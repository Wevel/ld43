﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sound : MonoBehaviour
{
    [System.Serializable]
    public class SoundFX
    {
        public string name;
        public float volumeModifier = 1f;
        public AudioClip[] clips;
                
        [System.NonSerialized]
        public AudioSource source;

        public void PlayRandomClip ()
        {
            if (clips.Length > 0)
            {
                if (!source.isPlaying)
                {
                    source.clip = clips[Random.Range (0, clips.Length)];
                    source.Play ();
                }
            }
            else
            {
                Debug.LogError ("No clips for fx: " + name);
            }
        }
    }

    private static Sound instance;

    public static void PlayFX (string name)
    {
        instance.playFX (name);
    }

    public SoundFX music;
    public List<SoundFX> fx = new List<SoundFX> ();

    public AudioSource musicAudioSource;
    public AudioSource fxSourcePrefab;
    public Slider volumeSlider;

    private float volume;

    private void Awake ()
    {
        for (int i = 0; i < fx.Count; i++)
        {
            fx[i].source = Instantiate (fxSourcePrefab, transform);
            fx[i].source.gameObject.name = "AudioSource: " + fx[i].name;
        }
        music.source = musicAudioSource;
    }

    private void Update ()
    {
        volume = volumeSlider.value;
        music.source.volume = volume * music.volumeModifier; ;
        for (int i = 0; i < fx.Count; i++) fx[i].source.volume = volume * fx[i].volumeModifier;

        music.PlayRandomClip ();
    }

    private void OnEnable ()
    {
        instance = this;
    }

    private void OnDisable ()
    {
        instance = null;
    }

    private void playFX (string name)
    {
        int index = fx.FindIndex (x => x.name == name);
        if (index < 0) Debug.LogError ("No sound fx with name: " + name);
        else fx[index].PlayRandomClip ();
    }
}
