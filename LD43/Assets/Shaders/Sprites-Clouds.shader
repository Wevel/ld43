Shader "Sprites/Clouds"
{
    Properties
    {
		_CloudTex ("Cloud Texture", 2D) = "white" {}
		_CloudTex2 ("Cloud Texture", 2D) = "white" {}
		_PixelSize ("Pixel Size", Float) = 0.125
		_CloudCutoff ("Cloud Cutoff", Float) = 0.5
		_Speed ("Speed", Float) = 0.5
		_TimeOffset ("Time Offset", Float) = 0
	}

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float2 worldPosition : TEXCOORD1;
			};

			v2f vert (appdata_t IN)
			{
				v2f OUT;
						
				OUT.vertex = UnityObjectToClipPos (IN.vertex);
				OUT.worldPosition = IN.vertex;
				OUT.vertex = UnityPixelSnap (OUT.vertex);

				return OUT;
			}

			sampler2D _CloudTex;
			float4 _CloudTex_TexelSize;
			float4 _CloudTex_ST;

			sampler2D _CloudTex2;
			float4 _CloudTex2_TexelSize;
			float4 _CloudTex2_ST;

			float _PixelSize;
			float _CloudCutoff;
			float _Speed;
			float _TimeOffset;

			fixed4 frag (v2f IN) : SV_Target
			{
				float2 position = IN.worldPosition;
				position.x += floor (_Speed * _TimeOffset / _PixelSize) * _PixelSize;

				float2 position2 = float2 (position.x, -position.y);

				int worldX = floor (position.x / _PixelSize);
				int worldY = floor (position.y / _PixelSize);

				if (worldY <= 6) return 0;
				float cloud1 = tex2D (_CloudTex, TRANSFORM_TEX (position, _CloudTex)).r;
				float cloud2 = tex2D (_CloudTex2, TRANSFORM_TEX (position, _CloudTex2)).r;

				float cloud3 = tex2D (_CloudTex, TRANSFORM_TEX (position2, _CloudTex)).r;
				float cloud4 = tex2D (_CloudTex2, TRANSFORM_TEX (position2, _CloudTex2)).r;

				float value = ((cloud1 + (cloud3 * 0.4)) + (cloud2 + (cloud4 * 0.4))) / 2.0;

				fixed4 c = step (_CloudCutoff, value) * (0.3 + (value * 0.7)) * (0.8 + (worldY / 80));
				c.a *= 0.5;

				c.rgb *= c.a;
				return c;
			}
        ENDCG
        }
    }
}
