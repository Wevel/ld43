Shader "Sprites/Ground"
{
    Properties
    {
		_NoiseTex ("Noise Texture", 2D) = "white" {}
		_NoiseScale ("Noise Scale", Float) = 1
		_PixelSize ("Pixel Size", Float) = 0.1
		_ColourA ("Colour A", Color) = (1,1,1,1)
		_ColourB ("Colour B", Color) = (1,1,1,1)
		_SkyColourA ("Sky Colour A", Color) = (1,1,1,1)
		_SkyColourB ("Sky Colour B", Color) = (1,1,1,1)
		_Increments ("Increments", Int) = 10
		_SkyIncrements ("Sky Increments", Int) = 10
		_HorizonHeight ("Horizon Height", Float) = 2
	}

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float2 worldPosition : TEXCOORD1;
			};

			v2f vert (appdata_t IN)
			{
				v2f OUT;
						
				OUT.vertex = UnityObjectToClipPos (IN.vertex);
				OUT.worldPosition = IN.vertex;
				OUT.vertex = UnityPixelSnap (OUT.vertex);

				return OUT;
			}

			sampler2D _NoiseTex;
			float4 _NoiseTex_TexelSize;
			float4 _NoiseTex_ST;

			float _NoiseScale;
			float _PixelSize;
			float _HorizonHeight;
			int _Increments;
			int _SkyIncrements;

			fixed4 _ColourA;
			fixed4 _ColourB;
			fixed4 _SkyColourA;
			fixed4 _SkyColourB;

			fixed4 frag (v2f IN) : SV_Target
			{
				int worldX = floor (IN.worldPosition.x / _PixelSize);
				int worldY = floor (IN.worldPosition.y / _PixelSize);

				float noise = tex2D (_NoiseTex, TRANSFORM_TEX (float2 (frac (worldX * _PixelSize), frac (frac(worldX / _NoiseTex_TexelSize.z) * _PixelSize)) * _NoiseTex_TexelSize.zw, _NoiseTex) * _NoiseTex_TexelSize.xy).r;

				fixed4 c = lerp (
					lerp (_ColourA, _ColourB, clamp (-worldY / (float)(_Increments + _HorizonHeight), 0, 1)), 
					lerp (_SkyColourA, _SkyColourB, clamp (worldY / (float)(_SkyIncrements + _HorizonHeight), 0, 1)),
					worldY + ((noise - 0.5) * 2.0 * _NoiseScale) > _HorizonHeight);

				c.rgb *= c.a;
				return c;
			}
        ENDCG
        }
    }
}
