Shader "Sprites/People/SimpleMapMode"
{
    Properties
    {
		_DataTex ("Data Texture", 2D) = "black" {}
		_PositionScale ("PositionScale", Float) = 1
		_WealthCount ("Wealth Count", Int) = 4
		_PixelSize ("Pixel Size", Float) = 0.1
		_LowColour ("Low Colour", Color) = (1,1,1,1)
		_HighColour ("High Colour", Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float2 worldPosition : TEXCOORD1;
			};

			v2f vert (appdata_t IN)
			{
				v2f OUT;
						
				OUT.vertex = UnityObjectToClipPos (IN.vertex);
				OUT.worldPosition = IN.vertex;
				OUT.vertex = UnityPixelSnap (OUT.vertex);

				return OUT;
			}

			sampler2D _DataTex;

			uint _PersonCount;
			uint _DataTextureSize;
			float _PositionScale;
			float _PixelSize;

			fixed4 _LowColour;
			fixed4 _HighColour;

			fixed4 frag (v2f IN) : SV_Target
			{
				int worldX = floor (IN.worldPosition.x / _PixelSize);
				int worldY = floor (IN.worldPosition.y / _PixelSize);

				bool isPerson = worldY == 1 || worldY == 0;

				fixed4 c = 0;

				if (isPerson)
				{
					float noise, wealth;
					float4 data;

					// Because we have tex2D commands, the compiler wants to unroll the loop. This doesn't work for a dynamic _PersonCount
					//  so we need to tell the compiler to keep this as a loop, even if it isn't very efficient
#if !SHADER_API_GLES
					[loop]
#endif
					for (uint i = 0; i < _PersonCount; i++)
					{
						data = tex2D (_DataTex, float2 (((i % _DataTextureSize) + 0.5) / (float)_DataTextureSize, (floor (i / (float)_DataTextureSize) + 0.5) / (float)_DataTextureSize));

						if (worldX == floor ((data.x - 0.5) * _PositionScale * 2 / _PixelSize))
						{
							c = lerp (_LowColour, _HighColour, data.g);
							break;
						}
					}
				}

				c.rgb *= c.a;
				return c;
			}
        ENDCG
        }
    }
}
